<?php
session_name("Isbank");
session_start();
session_unset();
session_destroy();

header("Location: ../../auth/sign-in.html");
?>