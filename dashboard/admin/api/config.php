<?php


$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mining_project";

// Create a new MySQL connection
$dbConnection = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($dbConnection->connect_error) {
    die("Connection failed: " . $dbConnection->connect_error);
}

?>
