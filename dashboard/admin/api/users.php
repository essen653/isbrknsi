<?php

use PHPMailer\PHPMailer\PHPMailer;
require_once 'config.php';
// error_reporting(E_ALL);
// ini_set('display_errors', 1);





class projectAPI{
    private $sqlConn;
    private $users;

    public function __construct($usersTable, $dbConnection){
        $this->sqlConn = $dbConnection;
        $this->users = $usersTable;            
                    
    }

    // User authentication: Signup
    public function signup($firstname, $lastname, $email, $password, $phone) {
        $stmt = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            return [
                'message' => 'Email already exists',
            ];
        }

        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

        $stmt = $this->sqlConn->prepare("INSERT INTO $this->users (firstname, lastname, email, password, phone) VALUES (?, ?, ?, ?, ?)");
        $stmt->bind_param("sssss", $firstname, $lastname, $email, $hashedPassword, $phone);

        if ($stmt->execute()) {
            $userId = $stmt->insert_id;
            return [
                'userId' => $userId,
                'message' => 'Signup successful',
            ];
        } else {
            return [
                'message' => 'Failed to register',
            ];
        }
    }
    public function isLoginValid($phone, $password) {
        $stmt = $this->sqlConn->prepare("SELECT user_id, password FROM `users` WHERE phone = ?");
        if (!$stmt) {
            return false; // Database error
        }
        
        $stmt->bind_param("s", $phone);
        $stmt->execute();
        $result = $stmt->get_result();
        
        if ($result->num_rows > 0) {
            $user = $result->fetch_assoc();
            return password_verify($password, $user['password']) ? $user['user_id'] : false;
        }
    
        return false; // Login credentials not found
    }

    // User authentication: Forgot Password
    public function forgotPassword($email) {
        $user = $this->users->findOne(['email' => $email]);

        if ($user) {
            // Generate and save reset token
            $resetToken = $this->generateRandomToken();
            $this->users->updateOne(
                ['email' => $email],
                ['$set' => ['reset_token' => $resetToken]]
            );

            // Sending email with the reset token
            $mail = new PHPMailer(true);
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'essen653@gmail.com';
            $mail->Password = 'yrumbywwifhqpwoa';
            $mail->Port = 465; 
            $mail->SMTPSecure = 'ssl';
            
            $mail->setFrom('essen653@gmail.com', 'Golden Laundry Service'); 
            $mail->addAddress($email); 
            
            $mail->Subject = 'Password Reset from Laundry API';
            $mail->Body = 'Your password reset token is ' . $resetToken;
            
            if ($mail->send()) {
                return true; 
            } else {
                return false; 
            }
        }

        // User not found
        return false;                                               
    }


    // User authentication: Reset Password
    public function resetPassword($email, $resetToken, $newPassword){
        $user = $this->users->findOne(['email' => $email, 'reset_token' => $resetToken]);

        if ($user) {
            // Update password
            $this->users->updateOne(
                ['email' => $email],
                ['$set' => ['password' => password_hash($newPassword, PASSWORD_DEFAULT), 'reset_token' => null]]
            );

            return true;
        }

        // Invalid token or user not found
        return false;
    }
    

    public function getItemStatus($requestNumber){
        $criteria = [
            'pickup_requests.request_number' => $requestNumber,
        ];
    
        // Execute the findOne query
        $user = $this->users->findOne($criteria);
    
        if ($user) {
            // Iterate over pickup requests to find the matching one
            foreach ($user['pickup_requests'] as $pickupRequest) {
                // Check if the request number matches
                if (isset($pickupRequest['request_number']) && $pickupRequest['request_number'] === $requestNumber) {
                    // Extract the item details
                    $status = $pickupRequest['status'];
                    $numberOfItem = $pickupRequest['number_of_item'];
                    $amount = $pickupRequest['amount'];
                    $userEmail = $user['email'];
    
                    // Create the item details array
                    $itemDetails = [
                        'number_of_item' => $numberOfItem,
                        'amount' => $amount,
                        'status' => $status,
                        'user_email' => $userEmail
                    ];
                    return $itemDetails;
                }
            }
        }
    
        // Request not found
        return 'Item not found';
    }
    

    // Make a pickup request
    public function makePickupRequest($userId, $numberOfItem, $phone, $pickupAddress){
        $user = $this->users->findOne(['_id' => new MongoDB\BSON\ObjectID($userId)]);


        if ($user) {
            // Create a new pickup request with a tracking code
            $requestNumber = $this->generateRandomToken();
            $totalCost = $numberOfItem * 700;
            $cost = "N700";
            $pickupRequest = [
                'request_number' => $requestNumber,
                'phone' => $phone,
                'address' => $pickupAddress,
                'delivery_date' => '',
                'number_of_item' => $numberOfItem,
                'amount' => $numberOfItem * 700,
                'date_created' => date('Y-m-d'),
                'status' => 'Pickup request sent. Waiting for approval',
            ];

            // Add the pickup request to the user's array of requests
            $this->users->updateOne(
                ['_id' => new MongoDB\BSON\ObjectID($userId)],
                ['$push' => ['pickup_requests' => $pickupRequest]]
            );
            // Sending email to the user with request number
            $email = $user['email'];
            $mail = new PHPMailer(true);
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'essen653@gmail.com';
            $mail->Password = 'yrumbywwifhqpwoa';
            $mail->Port = 465; 
            $mail->SMTPSecure = 'ssl';
            
            $mail->setFrom('essen653@gmail.com', 'Golden Laundry Service'); 
            $mail->addAddress($email); 
            
            $mail->Subject = 'Pickup Request Initiated';
            $mail->Body = 'Your request was sent successful with the following details: ' . 
                ' Request Number:' . $requestNumber .
                ' Address:' . $pickupAddress .
                ' Contact Number:' . $phone .
                ' Number of Items:' . $numberOfItem .
                ' Cost per Item: ' . $cost .
                ' Total Cost: ' . $totalCost;
            
            if ($mail->send()) {
                $name = $user['name'];
                $message = 'Pickup Request Initiated with the following details: <br>' .
                ' Name: ' . $name . '<br>' .
                ' Request Number : ' . $requestNumber . '<br>' .
                ' Address :' . $pickupAddress . '<br>' .
                ' Contact Number :' . $phone . '<br>' .
                ' Number of Items :' . $numberOfItem . '<br>' .
                ' Cost per Item :' . $cost . '<br>' .
                ' Total Cost :' . 'N'.$totalCost . '<br>' .
                ' Please check your mail for more details';
                return $message;
            } else {
                return false; 
            }


            return true;
        }

        // User not found
        return false;
    }


    //Generate random token to reset password
    //Generate request Number
    private function generateRandomToken($length = 6) {
        $characters = '0123456789ABCDE';
        $token = '';
        for ($i = 0; $i < $length; $i++) {
            $token .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $token;
    }

    //Handle all end-points
    public function handleRequest($method, $endpoint, $data){
        // echo $endpoint;
        switch ($endpoint) {
            case '/getItemStatus':
                if ($method === 'POST') {
                    if (isset($data['requestNumber'])) { 
                        $requestNumber = $data['requestNumber'];
                        $itemStatus = $this->getItemStatus($requestNumber);
                        if ($itemStatus) {
                            // Item found, return the item details
                            return json_encode($itemStatus);
                        } else {
                            // Item not found
                            return 'Item not found';
                        }
                    } else {
                        return 'Missing requestNumber parameter'; 
                    }
                }
                break;
            
            case '/makePickupRequest':
                if ($method === 'POST') {
                    if (isset($data['userID']) && ($data['numberOfItem']) && ($data['phone']) && ($data['pickupAddress'])) { 
                        $userId = ($data['userID']);
                        $numberOfItem = ($data['numberOfItem']);
                        $phone = ($data['phone']);
                        $pickupAddress = ($data['pickupAddress']);
                        $details = $this->makePickupRequest($userId, $numberOfItem, $phone, $pickupAddress);
                        if ($details) {
                            return json_encode($details);
                        } else {
                            return 'Request failed to send';
                        }
                    } else {
                        return 'Missing parameters'; 
                    }
                }
                break;
            case '/signup':
                if ($method === 'POST') {
                    if (isset($data['firstname']) && isset($data['lastname']) && isset($data['email']) && isset($data['password'])) {
                        $email = $data['email'];
    
                        // Checking if email already exists
                        $stmt = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE email = ?");
                        $stmt->bind_param("s", $email);
                        $stmt->execute();
                        $result = $stmt->get_result();
    
                        if ($result->num_rows > 0) {
                            return 'Email already exists';
                        } else {
                            $firstname = $data['firstname'];
                            $lastname = $data['lastname'];
                            $email = $data['email'];
                            $password = $data['password'];
                            $phone = isset($data['phone']) ? $data['phone'] : '';
                            $details = $this->signup($firstname, $lastname, $email, $password, $phone);
                            if ($details['userId']) {
                                $user_id = $details['userId'];
                                // Request approved successfully
                                return "User registration was successful. Your user ID is: $user_id";
                            } else {
                                // Request not found or unable to approve
                                return 'Failed to register';
                            }
                        }
                    } else {
                        return 'Missing parameters';
                    }
                } else {
                    return "Invalid request methods.";
                }
                break;
            case '/login':
                if ($method === 'POST') {
                    $data = json_decode(file_get_contents('php://input'), true); // Get POST data
                    if (isset($data['phone']) && isset($data['password'])) {
                        $phone = $data['phone'];
                        $password = $data['password'];
                        
                        $userId = $this->isLoginValid($phone, $password);
                        if ($userId) {
                            $newSessionID = bin2hex(random_bytes(16));
            
                            $updateStmt = $this->sqlConn->prepare("UPDATE users SET session_id = ? WHERE user_id = ?");
                            if ($updateStmt) {
                                $updateStmt->bind_param("si", $newSessionID, $userId);
                                $updateStmt->execute();
            
                                // Set session ID and user ID cookies
                                setcookie('sessionID', $newSessionID, time() + 7 * 24 * 60 * 60, '/'); // Expires in 7 days
                                setcookie('userID', $userId, time() + 7 * 24 * 60 * 60, '/'); // Expires in 7 days
            
                                $response = [
                                    'message' => 'Login was successful',
                                ];
                            } else {
                                $response = ['message' => 'Database error'];
                            }
                        } else {
                            $response = ['message' => 'Login Failed'];
                        }
                    } else {
                        $response = ['message' => 'Missing parameters'];
                    }
            
                    header('Content-Type: application/json');
                    echo json_encode($response);
                    exit; // Add this line to terminate further processing
                }
                break;
            case '/forgotPassword':
                if ($method === 'POST') {
                    if (isset($data['email'])){ 
                        $email = $data['email'];
                        $p_email = $this->forgotPassword($email);
                        if ($p_email) {
                            return 'Forgot password token was sent to your email';
                        } else {
                            return 'Failed to send token';
                        }
                    } else {
                        return 'Missing parameters'; 
                    }
                }
                break;
            case '/resetPassword':
                if ($method === 'POST') {
                    if ($data['resetToken'] == null) {
                        if (isset($data['email']) && ($data['resetToken']) && ($data['newPassword'])){ 
                        $email = $data['email'];
                        $resetToken = $data['resetToken'];
                        $newPassword = $data['newPassword'];
                        $p_email = $this->resetPassword($email, $resetToken, $newPassword);
                        if ($p_email) {
                            return 'You\'ve successfully reset your password ';
                        } else {
                            return 'Password reset failed. Please confirm your Token';
                        }
                        } else {
                            return 'Missing parameters'; 
                        }
                    } else {
                        return 'Failed. Please go to forgot password to generate reset token.';
                    }
                    
                }
                break;
                         

            default:
                return 'Invalid endpoint';
        }
    }
    
}


// MongoDB configuration
// $dbName = 'mining_project';
$usersTable = 'users';


$api = new projectAPI($usersTable, $dbConnection);

$method = $_SERVER['REQUEST_METHOD'];
$endpoint = parse_url($_SERVER['PATH_INFO'], PHP_URL_PATH);
$endpoint = rtrim($endpoint, '/');



$data = $_POST; 

// Handle the request
// var_dump($data);
$response = $api->handleRequest($method, $endpoint, $data);

// Set the appropriate headers
header('Content-Type: application/json');

// Send the response
echo json_encode($response);

