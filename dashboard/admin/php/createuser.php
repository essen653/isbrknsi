<?php
$mysqli = new mysqli('localhost', 'isbr_isbrka', 'Dinoboy123', 'isbr_isbrka');
error_reporting(E_ALL);
ini_set('display_errors', 1);

if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: " . $mysqli->connect_error;
    exit();
}

$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$phone = '+9********';
$email = $_POST['email'];
$password = $_POST['password'];
$country = $_POST['country'];

$characters = '1234567890';
$customerid = '';
for ($i = 0; $i < 6; $i++) {
    $customerid .= $characters[rand(0, strlen($characters) - 1)];
}

$characters1 = '1234567890';
$account = '';
for ($i = 0; $i < 12; $i++) { // Changed $12 to 12
    $account .= $characters[rand(0, strlen($characters) - 1)];
}

$stmt = $mysqli->prepare("INSERT INTO users (firstname, lastname, email, password, phone, country, customerid, account_number) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
$stmt->bind_param("ssssssss", $firstname, $lastname, $email, $password, $phone, $country, $customerid, $account);

// Execute the statement
if ($stmt->execute()) {
    echo "Records inserted successfully.";
} else {
    echo "Error: " . $stmt->error;
}

// Close the statement and connection
$stmt->close();
$mysqli->close();
?>
